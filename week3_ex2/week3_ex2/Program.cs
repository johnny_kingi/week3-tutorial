﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week3_ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = new string[5] { "red", "blue", "orange", "white", "black" };

            foreach (var i in colour)
            {
                Console.WriteLine($"Colour is {i}");
            }

        }
    }
}
