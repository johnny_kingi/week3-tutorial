﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = new string[5] { "red", "blue", "orange", "white", "black" };
            Array.Sort(colour);
            Array.Reverse(colour);

            Console.WriteLine(string.Join(",", colour));



        }
    }
}
